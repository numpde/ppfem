ppfem
=====

R. Andreev, 2016-09-01

**ppfem** is a collection of MATLAB routines for 
the finite element method (FEM) with piecewise polynomial splines
(aka isogeometric analysis)
in 1d.
The idea of the package is to facilitate prototyping 
on tensor product meshes --- at the possible cost of assembly speed.

* For maximum flexibility, 
the basis functions are constructed explicitly and 
stored in the MATLAB [ppform](https://mathworks.com/help/curvefit/the-ppform.html).
* Spaces based on different 
spatial meshes and local polynomial degrees can be mixed, 
for instance in constructing Gramians.
* Some common spline spaces are provided (B-splines, continuous piecewise polynomials, Babuska--Shen, ...).
* Homogeneous boundary conditions of any kind and order can be easily implemented.
* Load vectors for n-variate functions on product meshes can be constructed.

Dependencies: 

* MATLAB [curvefit toolbox](https://mathworks.com/help/curvefit/).

Installation:

* [Download](https://bitbucket.org/numpde/ppfem/downloads) the repository.
* In MATLAB call the command addpath('..../ppfem/lib/') with the appropriate path.
* Try one of the examples in ppfem/examples/

Tutorial:

* [**Here**](./doc/).

License:

* [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

Warranties: 

* None.

