function poisson1d
% function poisson1d
%
%   Solve the 1d boundary value problem
%       -eps u'' + u = f,  u(-1) = 0, u'(+1) = 0
%   with C1 splines
%
%   Requires the path to ppfem:
%       addpath('../../lib/')

%   R. Andreev, 20160503

% Mesh
mesh = linspace(-1, 1, 17);

% Define trial space (C1 B-spline basis)
P = 2; % Polynomial degree
X = ppfem_construct(mesh, P, 'B0');

% Derivative operator
D = @(X) ppfem_transform(X, @fnder);

% Implement essential boundary conditions:
% Dirichlet left
X = ppfem_homconstraint(X, ppfem_eval(X, min(X.x)));
% Neumann right
X = ppfem_homconstraint(X, ppfem_eval(D(X), max(X.x)));

% Plot the basis
figure(2); hold on;
for n = 1 : X.dim
    xx = linspace(min(X.x), max(X.x), 1e3);
    plot(xx, ppfem_eval(X, xx) * ([1 : X.dim]' == n), '-');
    % Alternatively:
    %plot(xx, pp0val(X.pp{n}, xx), '-');
end
xlabel('x');
hold off;

% Define test space
Y = ppfem_construct(mesh, 0, 'D');

% Right hand side
f = @(x) (1 + x).^2 .^ (1 - x);
b = ppfem_assemload(f, {Y});

% System matrix for (-eps d_xx u + u) = f
eps = 1e-1;
A = -eps * ppfem_gramian(D(D(X)), Y) + ppfem_gramian(X, Y);

% Compute the discrete solution
u = A \ b;

% Plot the discrete solution
figure(1); hold on;
xx = linspace(min(mesh), max(mesh), 1e3);
plot(xx, ppfem_eval(X, xx) * u, 'b-');
% .. and the right hand side that it generates
plot(xx, (-eps * ppfem_eval(D(D(X)), xx) + ppfem_eval(X, xx)) * u, '-', xx, f(xx), '--');
legend('u ', 'A u ', 'f '); xlabel('x');
hold off;

end

