function main
% function main
%
%   Implements the space-time algorithm
%   for the heat equation from [1]
%   with arbitrary polynomial degree in time
%
%   [1] R. Andreev
%       Space-time discretization of the heat equation
%       Numer Algorithms, 67(4), 2014

%   R. Andreev, May 2016

    % Spatial mesh
    meshx = linspace(-1, 1, 2^4 + 1);

    % Temporal mesh
    T = 2;
    mesht = linspace(0, T, 20);
    % Refine in time towards the origin
    mesh_ref_at = @(m, t) sort(unique([m, t + min(nonzeros(abs(m-t))) * 2.^(-(0:10))]));
    mesht = mesh_ref_at(mesht, min(mesht));

    % Define univariate FEM spaces
    d = 3; % Temporal polynomial degree
    % Meaningful combinations for collocation RK are:
    %   'P(d) x D(d) x ...' for Gauss--Legendre
    %	'P(d) x R(d) x ...' for Gauss--Radau
    SPACES = sprintf('P(%d) x R(%d) x P(2)/0', d, d);
    disp(['Constructing spaces: ' SPACES]);
    [E, F, V] = FEM_N(SPACES, {mesht, mesht, meshx});
    
    % Univariate FEM space derivative operator
    D = @(X) ppfem_transform(X, @fnder);

    % Temporal FEM matrices
    disp('Constructing temporal FEM matrices');
    MtE = E.M;                        % trail  x trial
    AtE = ppfem_gramian(D(E));        % trial' x trial'
    MtF = F.M;                        % test   x test
    MtFE = ppfem_gramian(F, E);       % test   x trial
    CtFE = ppfem_gramian(F, D(E));    % test   x trial'
    E0 = ppfem_eval(E, min(mesht));   % trial(t0)
    
    % Spatial FEM matrices
    disp('Constructing spatial FEM matrices');
    Mx = V.M;
    Ax = ppfem_gramian(D(V));

    % Space-time operators
    disp('Defining space-time operators');
    B  = @(w) { (Mx  *  w   * CtFE') + (Ax  *  w   * MtFE'),   Mx  *  w   * E0' };
    Bt = @(v)   (Mx' * v{1} * CtFE ) + (Ax' * v{1} * MtFE ) + (Mx' * v{2} * E0)  ;
    iN = @(d) { Ax \ (d{1} / MtF), Mx \ d{2} };

    % Preconditioner M -- inverse
    disp('Defining space-time preconditioner');
    [VtE, DtE] = eig(full(AtE), full(MtE));
    gamma = sqrt(max(0, diag(DtE)));
    function iMw = iM(w)
        iMw = w * VtE;
        for j = 1 : E.dim
            iMw(:,j) = real((Ax + 1i * gamma(j) * Mx) \ iMw(:,j));
        end
        iMw = iMw * VtE';
    end

    % Assemble the space-time load vector
    disp('Assembling the space-time load vector');
    f = @(t, x) t .* ones(size(x));
    g = @(x) (x - min(meshx)) .* (max(meshx) - x);
    b = { reshape(ppfem_assemload(f, {F, V}), [V.dim, F.dim]), ppfem_assemload(g, {V}) };

    % Solve
    disp('Running the linear solver');
    tol = 1e-4; maxit = 100;
    [u, flag, ~, niter] = glsqr(B, Bt, b, tol, maxit, @iM, iN);
    disp(['LSQR finished after ' num2str(niter) ' iterations with flag ' num2str(flag)]);

    % Plot the discrete solution
    plot_u = @(u) plot_XxY(u', {V, E});
    plot_u(u);
    xlabel('x'); ylabel('t');
end

% Convert a string like '0' to a boundary condition operator
function BC = get_BC(type)
    switch type
        case '0' % Homogeneous Dirichlet BCs
            BC = @(S) ppfem_homconstraint(S, ppfem_eval(S, S.x([1,end])));
        case '1' % Homogeneous Neumann BCs
            BC = @(S) ppfem_homconstraint(S, ppfem_eval(ppfem_transform(S, @fnder), S.x([1,end])));
        case '8' % Periodic C1
            BC_per_r = @(S,r) ppfem_homconstraint(S, diff(ppfem_eval(ppfem_transform(S, @(p)fnder(p,r)), S.x([1,end]))));
            BC = @(S) BC_per_r(BC_per_r(S,0),1);
        case '-' % No BCs
            BC = @(S) S;
        otherwise
            error(['Unknown boundary condition type: ' type]);
    end
end

% Parse e.g. types = 'P(1) x B(2)/1 x B(2)/1' to construct the FEM spaces
function varargout = FEM_N(types, meshes)
    % Parse e.g. type = 'P(1)/0' to produce the 1d FEM space
    function S = FEM_1(type, mesh)
        spec = strsplit(strtrim(type), '/'); % spec has length at least 1
        TP = spec{1}; TP = struct('ppfemtype', TP(1), 'degree', eval(TP(2:end)));
        if (length(spec) > 1); bc = get_BC(spec{2}); else bc = @(x)x; end
        S = bc(ppfem_construct(mesh, TP.degree, TP.ppfemtype));
        S.M = ppfem_gramian(S);
    end
    varargout = cellfun(@FEM_1, strsplit(types, 'x'), meshes, 'UniformOutput', false);
end

% The function handle plotter defaults to @surf
function h = plot_XxY(u, spaces, plotter)
    if (nargin < 3); plotter = @(varargin) surf(varargin{:}, 'LineStyle', 'none'); end
    [spacex, spacey] = spaces{:};
    ex = linspace(spacex.x(1), spacex.x(end), (109+1)); Ex = ppfem_eval(spacex, ex);
    ey = linspace(spacey.x(1), spacey.x(end), (107+1)); Ey = ppfem_eval(spacey, ey);
    h = plotter(ex, ey, Ey * reshape(u, [spacey.dim, spacex.dim]) * Ex');
    colormap summer; colorbar;
end

