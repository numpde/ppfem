function plap1d
% function plap1d
%
%   Solve 
%       -p-Laplace u = f  on  (-1,1)
%   with 
%	u(-1) = 0 = u(+1)
%   by a fixed point iteration

    m = linspace(-1, 1, 41) .^ 3;
    p = 2.5;
    V = ppfem_construct(m, 2, 'P');
    V = ppfem_homconstraint(V, ppfem_eval(V, [min(m), max(m)]));
    dV = ppfem_transform(V, @fnder);
    f = ppfem_assemload(@(x)1, {V});
    
    % Initial guess
    u = ppfem_assemload(@(x)(min(m)-x).*(x-max(m)), {V});
    % Fixed point iteration
    for i = 1 : 30
        Row = @(X) X(:)';
        c = @(x) abs(ppfem_eval(dV, Row(x)) * u) .^ (p-2);

        A = vectorized(@(w)ppfem_gramian(dV, dV, w), c);
        du = (A \ f) - u;
        while (max(abs(du)) > 0.9 * max(abs(u))); du = du / 2; end
        u = u + du;
        
        xx = linspace(min(m), max(m), 1e3) .^ 3;
        plot(xx, c(xx), '-.', xx, ppfem_eval(V, xx) * u, '-');
        legend('|u_{old}''|^{p-2}', 'u_{new}');
        title(['i = ' num2str(i)]);
        pause(0.1);
    end
end

function R = vectorized(F, f)
% function R = vectorized(F, f)
%
%   f should be 
%       a function handle
%       that accepts a N x d matrix X
%       of N points in d-dimensional space
%
%   The result of g = f(X) should be 
%       either a tensor or a function handle 
%       such that g(n) returns f(X(n,:))
%
%   R is the result of F(f) computed as follows:
%       In a first pass, call F(c) with
%       a dummy c = @(varargin)0.
%       In each callback of c, append 
%       the row(s) [varargin{:}] to X.
%
%       Execute g = f(X).
%
%       In a second pass, call F(d) with
%       a function d which in n-th
%       callback returns g(n).


    X = []; function r = c(varargin); X(end+1,:) = [varargin{:}]; r = 0; end
    F(@c);

    g = f(X);

    n = 0; function r = d(varargin); n = n + 1; r = g(n); end
    R = F(@d);

    clear n X
end

