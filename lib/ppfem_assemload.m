function F = ppfem_assemload(f, spaces, ipiece)
% function F = ppfem_assemload(f, spaces, ipiece)
%
%   spaces = {X1, X2, ...}
%   f is a function handle f(x1, x2, ...)
%
%   F is a Nx1 vector with
%       x1 the slowest index, 
%       x2 the 2nd-slowest, etc.
%
%   ipiece is a function handle 
%       such that ipiece(p) evaluates 
%       the integral of the product (f * p) 
%       over the interval p.breaks,
%       where p is a ppform
%       with only one piece.
%
%       It defaults to a quadrature rule.

%   R. Andreev, 2015-08-21

    space = spaces{1}; 
    if (length(spaces) > 1)
        f = @(t) ppfem_assemload(@(varargin)f(t,varargin{:}), spaces(2:end));
    end
    
    sumover = @(index, g) sum(cell2mat(foreach(index, g)), 2);
    
    if (nargin < 3)
        ipiece = @(p) sumover(space.QR(p.breaks), @(xw) f(xw.x) * ppval(p, xw.x) * xw.w);
    end
    
    F = [];
    for p = eachof(space.pp)
        pieces = foreach(1:p.pieces, @(k) ppbrk(p, k));
        F = [F; sumover(pieces, ipiece)];
    end
end

function fran = foreach(ran, f)
    if (isnumeric(ran)); ran = num2cell(ran); end
    if (isstruct(ran)); apply = @arrayfun; else apply = @cellfun; end
    fran = apply(f, ran, 'UniformOutput', false);
end

function array = eachof(array)
    array = [array{:}];
end
