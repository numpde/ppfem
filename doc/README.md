**ppfem** -- a tutorial
=======================

Introduction
------------

**ppfem** is a set of MATLAB routines for the finite element method with splines in 1d.
It is built on top of the MATLAB [curvefit toolbox](https://mathworks.com/help/curvefit/).

The path to the **ppfem** subfolder `lib/` should be made known to MATLAB using the command `addpath`, e.g.

~~~
:::matlab
addpath('path-to-ppfem/lib/');
~~~


The examples of this tutorial are [here](./examples/),
and more advanced ones can be found [here](../examples/).


Discrete function spaces
------------------------

The central concept of **ppfem** is that of a piecewise polynomial space, described in terms of basis functions (linear independence does not necessarily hold, but we will use this terminology for simplicity). 
The space is typically constructed using `ppfem_construct(mesh, d, type)`. The function accepts a 1d mesh `mesh` as a row vector of nodes, the polynomial degree `d`, and a string `type`. The type can be, for example, `'P'` for Lagrangian basis functions, `'D'` for discontinuous functions, `'B'` for B-spline basis functions. For the complete list, see `help ppfem_construct`.


**Example 001:**

~~~
:::matlab
% Create the spatial mesh as a vector of nodes
mesh = linspace(-1, 1, 9);
% Piecewise polynomial degree
d = 2;
% Construct the P2 Lagrangian space
V = ppfem_construct(mesh, d, 'P');
~~~


The generated variable `V` is a struct that contains the fields

* `x` which is the mesh. For example, `isequal(V.x, mesh)` is true.
* `dim` number of basis functions in V.
* `pp` which is a cell array of length `V.dim`, where `V.pp{n}` is the n-th basis function in MATLAB [ppform](http://mathworks.com/help/curvefit/the-ppform.html).
* `QR` is a function handle to a quadrature rule, which will be discussed below.

One of the last statements in the definition of `ppfem_construct`
is the call of the form

~~~
:::matlab
V = ppfem_filldetails(V, mesh);
~~~

It appends the fields `x`, `dim` and `QR` to `V` based on the information contained in `V.pp` and `mesh`, making sure that `V.x` contains both the nodes of `mesh` and all the breaks of the basis functions in `V.pp`.


To visualize the basis functions contained in `V` consider the following example.
In it we sample the 4-th basis function at the locations `xx` and plot the result.
The **ppfem** function `pp0val` wraps the MATLAB function [ppval](http://mathworks.com/help/matlab/ref/ppval.html) with the difference that the spline defined by `pp` ([ppform](http://mathworks.com/help/curvefit/the-ppform.html)) evaluates to zero outside its breaks, i.e. outside the interval [`min(pp.breaks)`, `max(pp.breaks)`].

**Example 001':**

~~~
:::matlab
xx = linspace(min(V.x), max(V.x), 1e3);
pp = V.pp{4};
plot(xx, pp0val(pp, xx));
~~~



This visualization example is intended to illustrate the basic design of the function spaces, but it is not idiomatic to FEM. In FEM, elements of the function space are typically represented by a vector of coefficients. For example, the column vector of coefficients `v`, defined in

~~~
:::matlab
e = @(n) ([1:V.dim]' == n);
v = 2 * e(4) - 2 * e(5);
~~~

would represent the function "`sum_n v(n) V.pp{n}`", which in this case is 2x the 4-th basis function minus 2x the 5-th one. There are several ways to manipulate -- let's say plot -- this composite function. One could combine all the splines into one spline with `pp0scale` and `pp0add`, and use `pp0val` to evaluate it. Alternatively,

~~~
:::matlab
E = ppfem_eval(V, xx);
~~~

constructs a matrix of size `length(xx)`x`V.dim` such that 

~~~
:::matlab
E * v
~~~

is the column vector of the values of the composite function at the nodes `xx`. Thus, the following plots the function represented by `v`:

**Example 001'':**

~~~
:::matlab
e = @(n) ([1:V.dim]' == n);
v = 2 * e(4) - 2 * e(5);
E = ppfem_eval(V, xx);
plot(xx, E * v);
~~~



Here is the complete combined **[Example 001](./examples/example_001.m):**

~~~
:::matlab
mesh = linspace(-1, 1, 9);
d = 2;
V = ppfem_construct(mesh, d, 'P');

pp = V.pp{4};

e = @(n) ([1:V.dim]' == n);
v = 2 * e(4) - 2 * e(5);

xx = linspace(min(V.x), max(V.x), 1e3);
E = ppfem_eval(V, xx);
h = plot(xx, pp0val(pp, xx),  xx, E * v, '-.');
~~~

**[Output here](./examples/images/example_001.png)**.


If the function represented by the vector `v` is discontinuous, it might be desirable to specify how it should be evaluated at a point of possible discontinuity `t`. For example, `ppfem_eval(V, t, '+') * v;` would evaluate the limit from the right and `ppfem_eval(V, t, 'j') * v;` would evaluate the jump. See

	help ppfem_eval

for more.



Gramians
--------

Let's define two spaces `U` and `V` by

~~~
:::matlab
mesh = linspace(-1, 1, 9);
U = ppfem_construct(mesh, 0, 'D');
V = ppfem_construct(mesh, 1, 'P');
~~~

Then

~~~
:::matlab
M = ppfem_gramian(U, V);
~~~

is the gramian matrix of size `U.dim` x `V.dim` in sparse format whose ij-th entry is the integral of the product of the basis functions `U.pp{i}` and `V.pp{j}`. Since the basis functions are piecewise polynomials, the integral is computed exactly. The following example illustrates how to project (in L^2) from the space `V` of continuous piecewise linear functions onto the space `U` of piecewise constant functions on a finer mesh, and plot the result.

**[Example 002](./examples/example_002.m):**

~~~
:::matlab
mesh = linspace(-1, 1, 9);
refine = @(m) interp1(1:length(m), m, 1:1/2:length(m));

withmass = @(A) setfield(A, 'M', ppfem_gramian(A));
V = withmass(ppfem_construct(mesh, 1, 'P'));
U = withmass(ppfem_construct(refine(mesh), 0, 'D'));

M = ppfem_gramian(U, V);

v = randn(V.dim, 1);
u = U.M \ (M * v);

xx = linspace(min(mesh), max(mesh), 1e3);
E = @(A) ppfem_eval(A, xx);
h = plot(xx, E(V) * v,  xx, E(U) * u);
~~~

**[Output here](./examples/images/example_002.png).**

The [anonymous function](http://mathworks.com/help/matlab/matlab_prog/anonymous-functions.html) `withmass` appends the field `M` to the space, which is the gramian of the space with itself, aka the mass matrix. See also [setfield](http://mathworks.com/help/matlab/ref/setfield.html).



The function `ppfem_gramian` also accepts a third parameter `w` which is a function handle to the integration weight. In this case, a quadrature rule is used to compute the entries of the gramian. This is implemented internally as follows:

- First, all nontrivial products of `pi = U.pp{i}` and `pj = V.pp{j}` are collected into a new function space `Z`. The product is obtained in [ppform](http://mathworks.com/help/curvefit/the-ppform.html) using `pp0cmb(pi, '*', pj);`, which extends [fncmb](http://mathworks.com/help/curvefit/fncmb.html#moreabout) by zero outside the breaks in the same way that `pp0val` above extends [ppval](http://mathworks.com/help/matlab/ref/ppval.html).
- As before, the routine `ppfem_filldetails` attaches a quadrature rule to the space `Z`.
- A load vector (see below) of the weight `w` is constructed with respect to the space `Z` using that quadrature rule.
- The entries of the load vector are filled into the gramian.

Load vectors
------------

Besides the function spaces and gramians, the load vector is the third main ingredient for FEM. Again, we begin by constructing the space `V`:

~~~
:::matlab
withmass = @(A) setfield(A, 'M', ppfem_gramian(A));

mesh = linspace(-1, 1, 21);
V = withmass(ppfem_construct(mesh, 2, 'P'));
~~~

Now let `f` be a function handle, say

~~~
:::matlab
f = @(x) sin(x * 3*pi);
~~~

The load vector

~~~
:::matlab
b = ppfem_assemload(f, {V});
~~~

is a column vector of length `V.dim` whose n-th component is the integral of `f` agains the n-th basis function `V.pp{n}` of the space `V`. The following example compares the original function `f` and the one obtained from the load vector `b` for function spaces `V` of different polynomial degrees.

**[Example 003](./examples/example_003.m):**

~~~
:::matlab
mesh = linspace(-1, 1, 9);

xx = linspace(min(mesh), max(mesh), 1e3);
E = @(A) ppfem_eval(A, xx);

% Define and plot the original function
f = @(x) sin(x * 3*pi);
h = plot(xx, f(xx), '-'); hold on;

% Construct a number of function spaces
VV = {};
for p = 0 : 2
    withmass = @(A) setfield(A, 'M', ppfem_gramian(A));
    VV{end+1} = withmass(ppfem_construct(mesh, p, 'D'));
end

% Loop over all spaces
for V = [VV{:}]
    % Project f in L2 onto the space V
    F = V.M \ ppfem_assemload(f, {V});
    % Plot the resulting function
    h(end+1) = plot(xx, E(V) * F, '-.');
end
~~~

**[Output here](./examples/images/example_003.png)**.


The routine `ppfem_assemload` also constructs load vectors from multivariate functions. Let's say we wish to project the function

~~~
:::matlab
f = @(x, y) cos(x) .* (y .^ 2);
~~~

defined on the rectangle (0,pi) x (-1,1), onto a space of nx x ny piecewise constants. This can be done as follows (most of the code deals with plotting). Please note carefully the order of x- and y-related arguments.


**[Example 004](./examples/example_004.m):**

~~~
:::matlab
function example_004
    % Number of elements
    nx = 6; ny = 5;

    meshx = linspace(0, pi, nx+1);
    meshy = linspace(-1, 1, ny+1);

    withmass = @(A) setfield(A, 'M', ppfem_gramian(A));
    X = withmass(ppfem_construct(meshx, 0, 'D'));
    Y = withmass(ppfem_construct(meshy, 0, 'D'));

    % Original
    f = @(x,y) cos(x) .* (y .^ 2);

    % Projection
    F = ppfem_assemload(f, {X, Y});
    F = Y.M \ reshape(F, [Y.dim, X.dim]) / X.M;

    % Plot the projection
    [xx, yy, h] = plot_XxY(F, {X, Y});
    
    % Plot the original
    hold on;
    h(end+1) = surf(xx, yy, f(xx, yy));

    xlabel('x'); ylabel('y');

    set(h(:), 'LineStyle', 'none');
    set(h(2), 'FaceAlpha', 0.6);
    colormap autumn;
end

function [xx, yy, h] = plot_XxY(F, XY)
    X = XY{1}; Y = XY{2};

    % Append a field for plotting
    X.q = linspace(min(X.x), max(X.x), 107);
    Y.q = linspace(min(Y.x), max(Y.x), 113);

    % Grid for plotting
    [yy, xx] = ndgrid(Y.q, X.q);
    assert(isequal(size(xx), [numel(Y.q), numel(X.q)]));
    
    % Plot the projection
    E = @(A) ppfem_eval(A, A.q);
    h = surf(xx, yy, E(Y) * F * E(X)');
end
~~~

**[Output here](./examples/images/example_004.png)**.



Taking derivatives
------------------

Define a function space by

~~~
:::matlab
mesh = linspace(-1, 1, 9);
V = ppfem_construct(mesh, 2, 'B');
~~~

and let

~~~
:::matlab
v = randn(V.dim, 1);
~~~

represent a random function therein. The derivative of this function will live in a different space which can be constructed by taking derivatives of each basis function. The MATLAB function [fnder](http://mathworks.com/help/curvefit/fnder.html) does this for individual splines in [ppform](http://mathworks.com/help/curvefit/the-ppform.html). The **ppfem** routine `ppfem_transform` can be used to apply it to each basis function:

~~~
:::matlab
D = @(A) ppfem_transform(A, @fnder);
dV = D(V);
~~~

Note that the basis functions `dV.pp{n}` of resulting space `dV` may not be linearly independent anymore (or could be even equal to zero). Now `v`, understood as the vector of coefficients with respect to the space `dV`, represents the derivative of the original function. 
The following example plots both functions.


**[Example 005](./examples/example_005.m):**

~~~
:::matlab
mesh = linspace(-1, 1, 9);
V = ppfem_construct(mesh, 2, 'B');

v = randn(V.dim, 1);

D = @(A) ppfem_transform(A, @fnder);
dV = D(V);

xx = linspace(min(mesh), max(mesh), 1e3);
E = @(A) ppfem_eval(A, xx);
h = plot(xx, E(V) * v, '-',  xx, E(dV) * v, '-.');
~~~


**[Output here](./examples/images/example_005.png)**.




Boundary conditions
-------------------

The basic rationale for implementing homogeneous boundary conditions in **ppfem** 
is building them directly into the function space (as essential boundary conditions)
by specifying an operator that vanishes on the desired subspace.

Define a function space `V` of B-splines of piecewise polynomial degree 2 by

~~~
:::matlab
mesh = linspace(-1, 1, 7);
d = 2;
V = ppfem_construct(mesh, d, 'B');
~~~

Let's say we wish to extract the subspace `V0` of `V` of periodic functions on the interval (-1, 1).
To that end we define the evaluation matrix

~~~
:::matlab
E = ppfem_eval(V, mesh([1,end]));
~~~

at the endpoints of the interval. This is a `2` x `V.dim` matrix. 
The desired subspace contains those functions with coefficient vectors `v` for which `diff(E) * v` vanishes.
That subspace is identified by the call

~~~
:::matlab
V0 = ppfem_homconstraint(V, diff(E));
~~~

The dimension (= the number of basis functions) of resulting space `V0` is one less compared to `V`. 
One of the boundary functions has been "glued" to the other. 
Compare 
[the original set of basis functions](./examples/images/example_006a.png)
with
[the new one](./examples/images/example_006b.png).

The basis functions in this new space still have C^1 smoothness inside the inverval (-1,1) but the new basis function is only C^0 across the periodic boundary. We can impose periodicity of higher smoothness order as follows.


~~~
:::matlab
D = @(A) ppfem_transform(A, @fnder);
E1 = ppfem_eval(D(V0), mesh([1,end]));
V1 = ppfem_homconstraint(V0, diff(E1));
~~~

The new subspace `V1` carries C^1 periodic boundary conditions
(see [its basis functions](./examples/images/example_006c.png)).


**[Example 006](./examples/example_006.m):**

~~~
:::matlab
function example_006
    mesh = linspace(-1, 1, 5);
    d = 2;
    V = ppfem_construct(mesh, d, 'B');
    
    figure; hold on; 
    h = plot_basis(V);

    
    E0 = ppfem_eval(V, mesh([1,end]));
    V0 = ppfem_homconstraint(V, diff(E0));
    
    figure; hold on;
    fplot(@(x)0*x, [0,0]); % Hack to sync colors
    h = plot_basis(V0);
    
    
    D = @(A) ppfem_transform(A, @fnder);
    E1 = ppfem_eval(D(V0), mesh([1,end]));
    V1 = ppfem_homconstraint(V0, diff(E1));
    
    figure; hold on;
    fplot(@(x)0*x, [0,0]); % Hack to sync colors
    fplot(@(x)0*x, [0,0]); % Hack to sync colors
    h = plot_basis(V1);
end

function h = plot_basis(V)
    % Plot all basis functions of the space V
    h = [];
    for pp = [V.pp{:}]
        h(end+1) = fplot(@(x)pp0val(pp,x), V.x([1,end]));
        % Locate peak of the basis function:
        x = fminbnd(@(x)(-pp0val(pp,x)), pp.breaks(1), pp.breaks(end));
        text(x, pp0val(pp,x), '\leftarrow', 'FontSize', 20);
    end
end
~~~

**Output 
[ A ](./examples/images/example_006a.png),
[ B ](./examples/images/example_006b.png),
[ C ](./examples/images/example_006c.png)**.



In addition to the subspace `V0` that satisfies the homogeneous constraint represented by the matrix `E`, the routine `ppfem_homconstraint` returns two more objects:

~~~
:::matlab
[V0, R, W] = ppfem_homconstraint(V, E);
~~~

The matrix `R` describes how the space `V0` is constructed from `V`, specifically, the i-th basis function `V0.pp{i}` is obtained as `sum_j R(i,j) V.pp{j}`. This kind of linear recombination is offered by the **ppfem** routine

~~~
:::matlab
V0 = ppfem_recombine(R, V);
~~~

and internally, `ppfem_homconstraint` takes advantage of that. Since `V0` is a subspace of `V`, if we have a coefficient vector `v0` of a function in `V0`, it is also represented by the coefficient vector

~~~
:::matlab
v = R' * v0;
~~~

in the larger function space `V`. Similarly, if `M = ppfem_gramian(V)` is the gramian for `V` then

~~~
:::matlab
M0 = R * M * R';
~~~

is the same as `M0 = ppfem_gramian(V0);`.

The third output `W` of `ppfem_homconstraint` provides a representation of the elements of `V` that have been removed. Each column `w` of `W` is a coefficient vector with respect to the original function space `V` and `E * w` is nonzero. 
See

~~~
:::matlab
help ppfem_homconstraint
~~~

for more.



Poisson problem 1d
------------------

The following is a standard example of the Poisson problem -u'' = f on the interval (0,1), with the boundary conditions u(0) = p and u'(1) = q.
The variational formulation is obtained by testing with a function v and intergrating by parts.
The Dirichlet condition at x = 0 is imposed in the essential way by constructing a function g with g(0) = p and solving for (u-g) on the subspace of functions v with v(0) = 0.
The Neumann condition at x = 1 is imposed naturally (weakly) in the variational formulation.
We solve the problem with B-splines that admit second derivatives, in order to compare f with -u''.

**[Example 007](./examples/example_007.m):**

~~~
:::matlab
% Derivative operator
D = @(A) ppfem_transform(A, @fnder);

% FEM space
V = ppfem_construct(linspace(0, 1, 27), 2, 'B');

% Source term / right hand side
f = @(x) 2 * ones(size(x));
F = ppfem_assemload(f, {V});

% Boundary conditions
p = +1; % Dirichlet:   u(0) = p
q = -2; % Neumann: du/dx(1) = q

% Stiffness matrix
A = ppfem_gramian(D(V));

% Homogeneous constraint { v(0) = 0 }
E0 = ppfem_eval(V, V.x(1));
[~, R, W] = ppfem_homconstraint(V, E0);

% A particular function that satisfies the Dirichlet BC
du = W * ((E0 * W) \ p);

% Right hand side 
% (subtract the particular, add the Neumann term)
E1 = transpose(ppfem_eval(V, V.x(end)));
F = F - A * du + q * E1;
% Solve on the homogeneous subspace
u = R' * ((R * A * R') \ (R * F));
% Reconstitute the particular part
u = u + du;

% Check the boundary conditions
disp('Error in BC: ');
norm([p; q] - [E0; ppfem_eval(D(V), V.x(end))] * u)

% Plot the solution u, the source term and -u''
xx = linspace(min(V.x), max(V.x), 1e3);
E = @(A) ppfem_eval(A, xx);
h = plot(xx, E(V) * u, '-',  xx, f(xx), '-.',  xx, -E(D(D(V))) * u, '--');
~~~

The error in the boundary conditions is on the order of `1e-15`;
**[image output here](./examples/images/example_007.png)**.



Quadrature
----------

The routine `ppfem_gauleg(N, I)` constructs the Gauss--Legendre quadrature on `N` nodes on the interval `I = [a,b]`.
Calling

~~~
:::matlab
N = 2;
I = [0,1];
qr = ppfem_gauleg(N, I);
~~~

produces a struct array `qr` with fields `x` (quadrature nodes) and `w` (quadrature weights).
To iterate over the array one can do

~~~
:::matlab
for i = 1 : length(qr)
    fprintf('Node %f / weight %f \n', qr(i).x, qr(i).w);
end
~~~

or

~~~
:::matlab
for qr = qr
    fprintf('Node %f / weight %f \n', qr.x, qr.w);
end
~~~



Sometimes, the same type of quadrature rule is required on many intervals.
The call

~~~
:::matlab
N = 2;
QR = ppfem_gauleg(N);
~~~

produces a function handle `QR` which accepts an interval `I` and returns `qr` as above;
thus `QR(I)` is the same as `ppfem_gauleg(N, I)`, and one can iterate as in 

~~~
:::matlab
for qr = QR(I)
    fprintf('Node %f / weight %f \n', qr.x, qr.w);
end
~~~


Alternatively, if `I` is a `K` x `2` collection of intervals then

~~~
:::matlab
QR = ppfem_gauleg(N, I);
~~~

returns a `K` x `N` struct array with the fields `x` and `w`. Setting, for example,

~~~
:::matlab
X = [QR.x];
W = [QR.w];
~~~

gives the row vectors `X` and `W` of nodes and weights.




When a function space is constructed with `V = ppfem_construct(mesh, d, type)`, a Gauss--Legendre quadrature rule, typically with `N = d + 1` points, is attached to `V` as a field `QR`. When `ppfem_assemload` is called to assemble the load vector, it uses this quadrature rule.

See **[Example 008](./examples/example_008.m)**.


---

R. Andreev, 2016-12-25

