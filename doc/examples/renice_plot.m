function h = renice_plot(h)
% function h = renice_plot(h)
%   
%   h is a handle returned by PLOT and such
%
%   R. Andreev, Apr 2008 -- Aug 2016
%
%   License: CC BY 4.0
    
    set(0, 'DefaultTextInterpreter', 'tex');
    set(gca, 'TickLabelInterpreter', 'tex');
    set(gca, 'FontUnits', 'Normalized');
    set(gca, 'FontSize', 0.05);
    grid on;
    set(gca, 'MinorGridLineStyle', 'none');

    if (~isgraphics(h))
    elseif (isgraphics(h, 'line') | isgraphics(h, 'functionline'))
        set(h, 'LineWidth', 3);
        set(h, 'MarkerSize', 10);
    elseif (isgraphics(h, 'legend'))
        set(h, 'Interpreter', 'latex');
        set(h, 'FontSize', round(get(h, 'FontSize') * 1.2));
    end
    
    niceaxis;

end

function niceaxis(enlarge)
% function NICEAXIS(BORDER_MARGIN)
%
%   BORDER_MARGIN (optional)
%
%   R. Andreev, Apr 2008
%
%   License: CC BY 4.0

    if (nargin < 1); enlarge = 3; end;

    axis tight;
    v = axis;
    if strcmp(get(gca, 'XScale'), 'linear')
        v(1:2) = v(1:2) + enlarge * 0.05 * v(1:2) * [1 -1; -1 1];
    else
        v(1:2) = exp(log(v(1:2)) + enlarge * 0.05 * log(v(1:2)) * [1 -1; -1 1]);
    end
    if strcmp(get(gca, 'YScale'), 'linear')
        v(3:4) = v(3:4) + enlarge * 0.05 * v(3:4) * [1 -1; -1 1];
    else
        %v(3) = max([v(3) eps]);
        v(3:4) = exp(log(v(3:4)) + enlarge * 0.05 * log(v(3:4)) * [1 -1; -1 1]);
    end
    axis(v);

    grid on;
    set(gca, 'GridLineStyle', '-');
    set(gca, 'MinorGridLineStyle', 'none');

    set(gca, 'Color', [1 1 1]*0.99);
    set(gca, 'YColor', [1 1 1]*0.4, 'XColor', [1 1 1]*0.4);
    set(get(gca, 'XLabel'), 'Color', 'k');
    set(get(gca, 'YLabel'), 'Color', 'k');
    set(get(gca, 'ZLabel'), 'Color', 'k');

    pbaspect([(1+sqrt(5))/2 1 1]);
end