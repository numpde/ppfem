clear all; close all;

mesh = linspace(-1, 1, 9);

xx = linspace(min(mesh), max(mesh), 1e3);
E = @(A) ppfem_eval(A, xx);

% Define and plot the original function
f = @(x) sin(x * 3*pi);
h = plot(xx, f(xx), '-'); hold on;

% Construct a number of function spaces
VV = {};
for p = 0 : 2
    withmass = @(A) setfield(A, 'M', ppfem_gramian(A));
    VV{end+1} = withmass(ppfem_construct(mesh, p, 'D'));
end

% Loop over all spaces
for V = [VV{:}]
    % Project f in L2 onto the space V
    F = V.M \ ppfem_assemload(f, {V});
    % Plot the resulting function
    h(end+1) = plot(xx, E(V) * F, '-.');
end

renice_plot(h);
saveas(gcf, 'images/example_003', 'png');
