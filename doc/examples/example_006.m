function example_006
    clear all; close all;

    mesh = linspace(-1, 1, 5);
    d = 2;
    V = ppfem_construct(mesh, d, 'B');
    
    figure; hold on; 
    h = plot_basis(V);

    renice_plot(h);
    saveas(gcf, 'images/example_006a', 'png');

    
    E0 = ppfem_eval(V, mesh([1,end]));
    V0 = ppfem_homconstraint(V, diff(E0));
    
    figure; hold on;
    fplot(@(x)0*x, [0,0]); % Hack to sync colors
    h = plot_basis(V0);
    
    renice_plot(h);
    saveas(gcf, 'images/example_006b', 'png');
    
    
    D = @(A) ppfem_transform(A, @fnder);
    E1 = ppfem_eval(D(V0), mesh([1,end]));
    V1 = ppfem_homconstraint(V0, diff(E1));
    
    figure; hold on;
    fplot(@(x)0*x, [0,0]); % Hack to sync colors
    fplot(@(x)0*x, [0,0]); % Hack to sync colors
    h = plot_basis(V1);

    renice_plot(h);
    saveas(gcf, 'images/example_006c', 'png');
end

function h = plot_basis(V)
    % Plot all basis functions of the space V
    h = [];
    for pp = [V.pp{:}]
        h(end+1) = fplot(@(x)pp0val(pp,x), V.x([1,end]));
        % Locate peak of the basis function:
        x = fminbnd(@(x)(-pp0val(pp,x)), pp.breaks(1), pp.breaks(end));
        text(x, pp0val(pp,x), '\leftarrow', 'FontSize', 20);
    end
end
