clear all; close all;

mesh = linspace(-1, 1, 9);
V = ppfem_construct(mesh, 2, 'B');

v = randn(V.dim, 1);

D = @(A) ppfem_transform(A, @fnder);
dV = D(V);

xx = linspace(min(mesh), max(mesh), 1e3);
E = @(A) ppfem_eval(A, xx);
h = plot(xx, E(V) * v, '-',  xx, E(dV) * v, '-.');

renice_plot(h);
saveas(gcf, 'images/example_005', 'png');
