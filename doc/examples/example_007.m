clear all; close all;

% Derivative operator
D = @(A) ppfem_transform(A, @fnder);

% FEM space
V = ppfem_construct(linspace(0, 1, 27), 2, 'B');

% Source term / right hand side
f = @(x) 2 * ones(size(x));
F = ppfem_assemload(f, {V});

% Boundary conditions
p = +1; % Dirichlet:   u(0) = p
q = -2; % Neumann: du/dx(1) = q

% Stiffness matrix
A = ppfem_gramian(D(V));

% Homogeneous constraint { v(0) = 0 }
E0 = ppfem_eval(V, V.x(1));
[~, R, W] = ppfem_homconstraint(V, E0);

% A particular function that satisfies the Dirichlet BC
g = W * ((E0 * W) \ p);

% Right hand side 
% (subtract the particular, add the Neumann term)
E1 = transpose(ppfem_eval(V, V.x(end)));
F = F - A * g + q * E1;
% Solve on the homogeneous subspace
u = R' * ((R * A * R') \ (R * F));
% Reconstitute the particular part
u = u + g;

% Check the boundary conditions
disp('Error in BC: ');
norm([p; q] - [E0; ppfem_eval(D(V), V.x(end))] * u)

% Plot the solution u, the source term and -u''
xx = linspace(min(V.x), max(V.x), 1e3);
E = @(A) ppfem_eval(A, xx);
h = plot(xx, E(V) * u, '-',  xx, f(xx), '-.',  xx, -E(D(D(V))) * u, '--');

renice_plot(h);
saveas(gcf, 'images/example_007', 'png');
