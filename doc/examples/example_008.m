clear all; close all;

QR = ppfem_gauleg(3);
qr = QR([0,1]);

disp('Variant 1');

for i = 1 : length(qr)
    fprintf('Node %f / weight %f \n', qr(i).x, qr(i).w);
end

disp('Variant 2');

for qr = qr
    fprintf('Node %f / weight %f \n', qr.x, qr.w);
end

disp('Variant 3');

for qr = QR([0,1])
    fprintf('Node %f / weight %f \n', qr.x, qr.w);
end

disp('Variant 4');


I = [[0,1]; [1,3]; [3,6]];
QR = ppfem_gauleg(2, I);
X = [QR.x];
W = [QR.w];
disp(X);
disp(W);

disp('V.QR');

mesh = linspace(-1, 1, 9);
V = ppfem_construct(mesh, 2, 'B');
for qr = V.QR([0,1])
    fprintf('Node %f / weight %f \n', qr.x, qr.w);
end
