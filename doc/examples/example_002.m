clear all; close all;

mesh = linspace(-1, 1, 9);
refine = @(m) interp1(1:length(m), m, 1:1/2:length(m));

withmass = @(A) setfield(A, 'M', ppfem_gramian(A));
V = withmass(ppfem_construct(mesh, 1, 'P'));
U = withmass(ppfem_construct(refine(mesh), 0, 'D'));

M = ppfem_gramian(U, V);

v = randn(V.dim, 1);
u = U.M \ (M * v);

xx = linspace(min(mesh), max(mesh), 1e3);
E = @(A) ppfem_eval(A, xx);
h = plot(xx, E(V) * v,  xx, E(U) * u);

renice_plot(h);
saveas(gcf, 'images/example_002', 'png');
