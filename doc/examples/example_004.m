function example_004
    close all;

    % Number of elements
    nx = 6; ny = 5;

    meshx = linspace(0, pi, nx+1);
    meshy = linspace(-1, 1, ny+1);

    withmass = @(A) setfield(A, 'M', ppfem_gramian(A));
    X = withmass(ppfem_construct(meshx, 0, 'D'));
    Y = withmass(ppfem_construct(meshy, 0, 'D'));

    % Original
    f = @(x,y) cos(x) .* (y .^ 2);

    % Projection
    F = ppfem_assemload(f, {X, Y});
    F = Y.M \ reshape(F, [Y.dim, X.dim]) / X.M;

    % Plot the projection
    [xx, yy, h] = plot_XxY(F, {X, Y});
    
    % Plot the original
    hold on;
    h(end+1) = surf(xx, yy, f(xx, yy));

    xlabel('x'); ylabel('y');

    set(h(:), 'LineStyle', 'none');
    set(h(2), 'FaceAlpha', 0.6);
    colormap autumn;

    renice_plot(0);
    saveas(gcf, 'images/example_004', 'png');
end

function [xx, yy, h] = plot_XxY(F, XY)
    X = XY{1}; Y = XY{2};

    % Append a field for plotting
    X.q = linspace(min(X.x), max(X.x), 107);
    Y.q = linspace(min(Y.x), max(Y.x), 113);

    % Grid for plotting
    [yy, xx] = ndgrid(Y.q, X.q);
    assert(isequal(size(xx), [numel(Y.q), numel(X.q)]));
    
    % Plot the projection
    E = @(A) ppfem_eval(A, A.q);
    h = surf(xx, yy, E(Y) * F * E(X)');
end
