clear all; close all;

mesh = linspace(-1, 1, 9);
d = 2;
V = ppfem_construct(mesh, d, 'P');

pp = V.pp{4};

e = @(n) ([1:V.dim]' == n);
v = 2 * e(4) - 2 * e(5);

xx = linspace(min(V.x), max(V.x), 1e3);
E = ppfem_eval(V, xx);
h = plot(xx, pp0val(pp, xx),  xx, E * v, '-.');

renice_plot(h);
saveas(gcf, 'images/example_001', 'png');
